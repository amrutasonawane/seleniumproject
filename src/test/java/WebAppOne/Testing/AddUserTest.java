package WebAppOne.Testing;

import org.testng.annotations.Test;
import org.testng.*;
import org.testng.AssertJUnit;
import static org.testng.Assert.assertEquals;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeTest;

public class AddUserTest {
	WebDriver driver ;
	String url = "";
@BeforeTest
public void SetUp()
{
	driver = new FirefoxDriver();
	url = "http://localhost:8888/c2t-WebApp1/users";
	driver.get(url);
	driver.findElement(By.linkText("Add User")).click();
}
@Test(priority=0)
public void TestAllLabels()
{
	List<WebElement> allLabel = driver.findElements(By.tagName("label"));
	AssertJUnit.assertEquals(allLabel.get(0).getText(), "Name");
	AssertJUnit.assertEquals(allLabel.get(1).getText(), "Email");
	AssertJUnit.assertEquals(allLabel.get(2).getText(), "Password");
	AssertJUnit.assertEquals(allLabel.get(3).getText(), "confirm Password");
	AssertJUnit.assertEquals(allLabel.get(4).getText(), "Address");
	AssertJUnit.assertEquals(allLabel.get(5).getText(), "Newsletter");
	AssertJUnit.assertEquals(allLabel.get(7).getText(), "Web Frameworks");
	AssertJUnit.assertEquals(allLabel.get(20).getText(), "Sex");
	AssertJUnit.assertEquals(allLabel.get(23).getText(), "Number");
	AssertJUnit.assertEquals(allLabel.get(34).getText(), "Country");
	AssertJUnit.assertEquals(allLabel.get(35).getText(), "Java Skills");
	
}
@Test(priority=1)
public void TestFirstName()
{
	driver.findElement(By.name("name")).clear();
	driver.findElement(By.tagName("button")).click();
	boolean iserror = driver.findElements(By.id("name.errors")).size()>0;
	if(iserror)
	{
		 WebElement firstNameError=driver.findElement(By.id("name.errors"));
		 assertEquals("Name is required!", firstNameError.getText());
	}
	else {
		org.testng.Assert.fail("something went wrong in firstname");
	}
}
@Test(priority=2)
public void TestEmailWithAll()
{
	driver.findElement(By.name("email")).clear();
	driver.findElement(By.tagName("button")).click();
	boolean iserror = driver.findElements(By.id("email.errors")).size()>0;
	if(iserror)
	{
		 WebElement email=driver.findElement(By.id("email.errors"));
		 assertEquals("Email is required!"
		 		+"\n"+ "Invalid Email format!", email.getText());
	}
	else {
		Assert.fail("something went wrong while testing email");
	}
}
@Test(priority=3)
public void TestEmailWithInvalidEmail()
{
	driver.findElement(By.name("email")).sendKeys("amrutagmail.com");
	driver.findElement(By.tagName("button")).click();
	boolean iserror = driver.findElements(By.id("email.errors")).size()>0;
	if(iserror)
	{
		 WebElement email=driver.findElement(By.id("email.errors"));
		 assertEquals("Invalid Email format!", email.getText());
	}else {
		Assert.fail("something went wrong while testing null values in email");
	}
}
@Test(priority=4)
public void TestPaassword()
{
	driver.findElement(By.name("password")).clear();
	driver.findElement(By.tagName("button")).click();
	boolean iserror = driver.findElements(By.id("email.errors")).size()>0;
	if(iserror)
	{
		 WebElement password=driver.findElement(By.id("password.errors"));
		 assertEquals("Password is required!", password.getText());
	}else {
		Assert.fail("something went wrong while testing password");
	}	
}
@Test(priority=5)
public void TestConfirmPaassword()
{
	driver.findElement(By.name("confirmPassword")).clear();
	driver.findElement(By.tagName("button")).click();
	boolean iserror = driver.findElements(By.id("confirmPassword.errors")).size()>0;
	if(iserror)
	{
		 WebElement confirmPassword=driver.findElement(By.id("confirmPassword.errors"));
		 assertEquals("Confirm password is required!", confirmPassword.getText());
	}else {
		Assert.fail("something went wrong while testing confirm password");
	}
}

@Test(priority=6)
public void TestPasswordAndConfirmPaassword()
{
	driver.findElement(By.name("password")).sendKeys("amruta");
	driver.findElement(By.name("confirmPassword")).sendKeys("amru");
	driver.findElement(By.tagName("button")).click();
	boolean iserror = driver.findElements(By.id("confirmPassword.errors")).size()>0;
	if(iserror)
	{
		 WebElement confirmPassword=driver.findElement(By.id("confirmPassword.errors"));
		 assertEquals("Passwords do not match, please retype!", confirmPassword.getText());
	}else {
		Assert.fail("something went wrong while testing confirm password");
	}
}
@Test(priority=7)
public void TestAddress()
{
	driver.findElement(By.name("address")).clear();
	driver.findElement(By.tagName("button")).click();
	boolean iserror = driver.findElements(By.id("address.errors")).size()>0;
	if(iserror)
	{
		 WebElement confirmPassword=driver.findElement(By.id("address.errors"));
		 assertEquals("Address is required!", confirmPassword.getText());
	}else {
		Assert.fail("something went wrong while testing confirm password");
	}
}
@Test(priority=8)
public void TestNewsLetterCheckbox()
{
	if(driver.findElement(By.id("newsletter")).isSelected());
	driver.findElement(By.id("newsletter")).click();
	driver.findElement(By.tagName("button")).click();
}
@Test(priority=9)
public void TestWebFrameworkCheckboxes()
{
	List<WebElement> elements = driver.findElements(By.name("framework"));
	for (int i = 0; i < elements.size(); i++) {
	     if(!elements.get(0).isSelected())
	    	 elements.get(0).click();
	     if(elements.get(1).isSelected())
	    	 elements.get(1).click();
	     if(elements.get(2).isSelected())
	    	 elements.get(2).click();
	     if(elements.get(3).isSelected())
	    	 elements.get(3).click();
	     if(elements.get(4).isSelected())
	    	 elements.get(4).click();
	     if(elements.get(5).isSelected())
	    	 elements.get(5).click();
	}
	driver.findElement(By.tagName("button")).click();
	WebElement framework = driver.findElement(By.id("framework.errors"));
    assertEquals("Please select at least two frameworks!", framework.getText());
}
@Test(priority=10)
public void TestCountry()
{
	Select selectCountry = new Select(driver.findElement(By.name("country")));
	selectCountry.selectByVisibleText("--- Select ---");
	driver.findElement(By.tagName("button")).click();
	WebElement framework = driver.findElement(By.id("country.errors"));
    assertEquals("Country is required!", framework.getText());
}
@Test(priority=11)
public void TestJavaskills()
{
	Select selectSkills = new Select(driver.findElement(By.name("skill")));
	selectSkills.deselectAll();
	selectSkills.selectByVisibleText("Hibernate");
	driver.findElement(By.tagName("button")).click();
	WebElement framework = driver.findElement(By.id("skill.errors"));
    assertEquals("Please select at least three skills!", framework.getText());
}

@Test(priority=12)
public void FirstNameTest() {
driver.findElement(By.name("name")).sendKeys("Amruta");	
}

@Test(priority=13)
public void EmailTest() {
	driver.findElement(By.name("email")).sendKeys("amruta10mar@gmail.com");
}

@Test(priority=14)
public void PasswordTest() {
	driver.findElement(By.name("password")).sendKeys("amruta123");

}
@Test(priority=15)
public void ConfirmPasswordTest() {
	
driver.findElement(By.name("confirmPassword")).sendKeys("amruta123");
}
@Test(priority=16)
public void AddressdTest() {
	driver.findElement(By.name("address")).sendKeys("mangal nagar wakad road");
}
@Test(priority=17)
public void NewsLetterTest() {
if(!driver.findElement(By.name("newsletter")).isSelected());
	driver.findElement(By.name("newsletter")).click();
}
@Test(priority=18)
public void WebFrameworksTest() {
 List<WebElement> frameworks = driver.findElements(By.name("framework"));
for(int i=0;i<frameworks.size();i++){
if(!frameworks.get(i).isSelected())
{
	frameworks.get(i).click();
}
}
}
@Test(priority=19)
public void GenderTest() {
driver.findElement(By.id("sex2")).click();
}
@Test(priority=20)
public void NumberTest() {
	driver.findElement(By.id("number5")).click();
}
@Test(priority=21)
public void CountryTest() {
new Select(driver.findElement(By.name("country"))).selectByVisibleText("United Stated");;
}
@Test(priority=22)
public void JavaskillsTest() {
	Select skillselect = new Select(driver.findElement(By.name("skill")));
skillselect.selectByIndex(0);
skillselect.selectByIndex(1);
skillselect.selectByIndex(2);
}
@Test(priority=23)
public void AddUserButtonTest() {
	driver.findElement(By.tagName("button")).click();
}
@Test(priority=24)
public void VerifyNumberOfRows()
{
	driver.navigate().to(url);
	List<WebElement> numberofrows = driver.findElements(By.tagName("tr"));
	int count = numberofrows.size();
	assertEquals(5,count);
}
}
