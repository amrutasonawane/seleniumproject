package WebAppOne.Testing;

import static org.testng.Assert.assertEquals;
import java.util.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class AddUserTableTest {
WebDriver driver ;
	@BeforeTest
	public void SetUp()
	{
		driver = new FirefoxDriver();
		String url = "http://localhost:8888/c2t-WebApp1/users";
		driver.get(url);
	}
	@Test
	public void VerifyNumberOfRows()
	{
		List<WebElement> numberofrows = driver.findElements(By.tagName("tr"));
		int count = numberofrows.size();
		assertEquals(4,count);
	}
	@Test
	public void VerifyNumberOfColumns()
	{
		List<WebElement> iColumns=new ArrayList<WebElement>();
		//List<WebElement> numberofcols = driver.findElements(By.tagName("td"));
		//int count = numberofcols.size();
		//assertEquals(6,count);
		for(int i=0;i<4;i++){
			iColumns = driver.findElements(By.xpath("html/body/div[1]/table/tbody/tr["+i+"]/td"));		
		}	
		int count = iColumns.size();
		assertEquals(5,count);
	}
	@Test 
	public void VerifyQueryButton()
	{
		WebElement QueryButton = driver.findElement(By.cssSelector("button.btn.btn-info"));
		String colorvalue =QueryButton.getCssValue("background-color");
		System.out.println("Quesry button ccolor"+colorvalue);  
		assertEquals("#5bc0de",String.format("#%02x%02x%02x", 91,192,222));
	}
	@Test
	public void CheckForClickableBtn()
	{
		WebElement UpdateButton = driver.findElement(By.xpath("html/body/div[1]/table/tbody/tr[1]/td[5]/button[2]"));		
		assertEquals(UpdateButton.isEnabled(),true);
	}
	
}
